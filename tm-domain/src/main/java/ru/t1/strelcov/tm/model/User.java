package ru.t1.strelcov.tm.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.strelcov.tm.enumerated.Role;

import java.util.Objects;

@Getter
@Setter
@NoArgsConstructor
public final class User extends AbstractEntity {
    @NotNull
    private String login;
    @NotNull
    private String passwordHash;
    @Nullable
    private String email;
    @Nullable
    private String firstName;
    @Nullable
    private String lastName;
    @Nullable
    private String middleName;
    @NotNull
    private Role role = Role.USER;
    @NotNull
    private Boolean lock = false;

    public User(@NotNull final String login, @NotNull final String passwordHash) {
        this.login = login;
        this.passwordHash = passwordHash;
    }

    public User(@NotNull final String login, @NotNull final String passwordHash, @Nullable final String email) {
        this.login = login;
        this.passwordHash = passwordHash;
        this.email = email;
    }

    public User(@NotNull final String login, @NotNull final String passwordHash, @Nullable Role role) {
        this.login = login;
        this.passwordHash = passwordHash;
        if (role == null) role = Role.USER;
        this.role = role;
    }

    @NotNull
    @Override
    public String toString() {
        return getId() + (login == null || login.isEmpty() ? "" : " : " + login);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;
        User user = (User) o;
        return login.equals(user.login) && passwordHash.equals(user.passwordHash) && Objects.equals(email, user.email) && Objects.equals(firstName, user.firstName) && Objects.equals(lastName, user.lastName) && Objects.equals(middleName, user.middleName) && role == user.role && lock.equals(user.lock);
    }

    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), login, passwordHash, email, firstName, lastName, middleName, role, lock);
    }

}
