package ru.t1.strelcov.tm.service;

import lombok.SneakyThrows;
import org.apache.ibatis.session.SqlSession;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.strelcov.tm.api.repository.IProjectRepository;
import ru.t1.strelcov.tm.api.repository.ITaskRepository;
import ru.t1.strelcov.tm.api.service.IConnectionService;
import ru.t1.strelcov.tm.api.service.IProjectService;
import ru.t1.strelcov.tm.enumerated.SortType;
import ru.t1.strelcov.tm.enumerated.Status;
import ru.t1.strelcov.tm.exception.empty.EmptyIdException;
import ru.t1.strelcov.tm.exception.empty.EmptyNameException;
import ru.t1.strelcov.tm.exception.entity.AccessDeniedException;
import ru.t1.strelcov.tm.exception.entity.EntityNotFoundException;
import ru.t1.strelcov.tm.exception.system.IncorrectSortOptionException;
import ru.t1.strelcov.tm.model.Project;

import java.util.Date;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.stream.Collectors;


public final class ProjectService extends AbstractBusinessService<Project> implements IProjectService {

    @NotNull
    protected final IConnectionService connectionService;

    public ProjectService(@NotNull final IConnectionService connectionService) {
        super(connectionService);
        this.connectionService = connectionService;
    }

    @NotNull
    @SneakyThrows
    @Override
    public Project add(@Nullable final String userId, @Nullable final String name, @Nullable final String description) {
        Optional.ofNullable(userId).orElseThrow(AccessDeniedException::new);
        Optional.ofNullable(name).filter((i) -> !i.isEmpty()).orElseThrow(EmptyIdException::new);
        final Project project;
        if (Optional.ofNullable(description).filter((i) -> !i.isEmpty()).isPresent())
            project = new Project(userId, name, description);
        else
            project = new Project(userId, name);
        add(project);
        return project;
    }

    @NotNull
    @SneakyThrows
    @Override
    public Project removeProjectWithTasksById(@Nullable final String userId, @Nullable final String projectId) {
        Optional.ofNullable(userId).orElseThrow(AccessDeniedException::new);
        Optional.ofNullable(projectId).filter((i) -> !i.isEmpty()).orElseThrow(EmptyIdException::new);
        @NotNull final SqlSession connection = connectionService.getSqlSession();
        @NotNull final ITaskRepository taskRepository = connection.getMapper(ITaskRepository.class);
        @NotNull final IProjectRepository projectRepository = connection.getMapper(IProjectRepository.class);
        try {
            taskRepository.removeAllByProjectId(userId, projectId);
            @NotNull final Project project = Optional.ofNullable(projectRepository.findById(projectId)).orElseThrow(EntityNotFoundException::new);
            projectRepository.remove(project);
            connection.commit();
            return project;
        } catch (@NotNull final Exception e) {
            connection.rollback();
            throw e;
        } finally {
            connection.close();
        }
    }

    @NotNull
    @SneakyThrows
    @Override
    public List<Project> findAll() {
        @NotNull final SqlSession connection = connectionService.getSqlSession();
        @NotNull final IProjectRepository projectRepository = connection.getMapper(IProjectRepository.class);
        try {
            return projectRepository.findAll();
        } finally {
            connection.close();
        }
    }

    @SneakyThrows
    @Override
    public void add(@Nullable final Project project) {
        if (project == null) return;
        @NotNull final SqlSession connection = connectionService.getSqlSession();
        @NotNull final IProjectRepository projectRepository = connection.getMapper(IProjectRepository.class);
        try {
            projectRepository.add(project);
            connection.commit();
        } catch (@NotNull final Exception e) {
            connection.rollback();
            throw e;
        } finally {
            connection.close();
        }
    }

    @SneakyThrows
    @Override
    public void addAll(@Nullable final List<Project> list) {
        @Nullable List<Project> listNonNull = Optional.ofNullable(list).map(l -> l.stream().filter(Objects::nonNull).collect(Collectors.toList())).orElse(null);
        if (listNonNull == null || listNonNull.size() == 0) return;
        @NotNull final SqlSession connection = connectionService.getSqlSession();
        @NotNull final IProjectRepository projectRepository = connection.getMapper(IProjectRepository.class);
        try {
            listNonNull.forEach(projectRepository::add);
            connection.commit();
        } catch (@NotNull final Exception e) {
            connection.rollback();
            throw e;
        } finally {
            connection.close();
        }
    }

    @SneakyThrows
    @Override
    public void clear() {
        @NotNull final SqlSession connection = connectionService.getSqlSession();
        @NotNull final IProjectRepository projectRepository = connection.getMapper(IProjectRepository.class);
        try {
            projectRepository.clear();
            connection.commit();
        } catch (@NotNull final Exception e) {
            connection.rollback();
            throw e;
        } finally {
            connection.close();
        }
    }

    @NotNull
    @SneakyThrows
    @Override
    public Project findById(@Nullable final String id) {
        Optional.ofNullable(id).filter((i) -> !i.isEmpty()).orElseThrow(EmptyIdException::new);
        @NotNull final SqlSession connection = connectionService.getSqlSession();
        @NotNull final IProjectRepository projectRepository = connection.getMapper(IProjectRepository.class);
        try {
            return Optional.ofNullable(projectRepository.findById(id)).orElseThrow(EntityNotFoundException::new);
        } finally {
            connection.close();
        }
    }

    @NotNull
    @SneakyThrows
    @Override
    public Project removeById(@Nullable final String id) {
        Optional.ofNullable(id).filter((i) -> !i.isEmpty()).orElseThrow(EmptyIdException::new);
        @NotNull final SqlSession connection = connectionService.getSqlSession();
        @NotNull final IProjectRepository projectRepository = connection.getMapper(IProjectRepository.class);
        try {
            final Project project = Optional.ofNullable(projectRepository.findById(id)).orElseThrow(EntityNotFoundException::new);
            projectRepository.remove(project);
            connection.commit();
            return project;
        } catch (@NotNull final Exception e) {
            connection.rollback();
            throw e;
        } finally {
            connection.close();
        }
    }

    @NotNull
    @SneakyThrows
    @Override
    public List<Project> findAll(@Nullable final String userId) {
        Optional.ofNullable(userId).orElseThrow(AccessDeniedException::new);
        @NotNull final SqlSession connection = connectionService.getSqlSession();
        @NotNull final IProjectRepository projectRepository = connection.getMapper(IProjectRepository.class);
        try {
            return projectRepository.findAllByUserId(userId);
        } finally {
            connection.close();
        }
    }

    @NotNull
    @SneakyThrows
    @Override
    public List<Project> findAll(@Nullable final String userId, @Nullable final String sort) {
        Optional.ofNullable(userId).orElseThrow(AccessDeniedException::new);
        Optional.ofNullable(sort).orElseThrow(() -> {
            throw new IncorrectSortOptionException(sort);
        });
        if (!SortType.isValidByName(sort)) throw new IncorrectSortOptionException(sort);
        @NotNull final SqlSession connection = connectionService.getSqlSession();
        @NotNull final IProjectRepository projectRepository = connection.getMapper(IProjectRepository.class);
        try {
            return projectRepository.findAllByUserIdSorted(userId, SortType.valueOf(sort).getDataBaseName());
        } finally {
            connection.close();
        }
    }

    @SneakyThrows
    @Override
    public void clear(@Nullable final String userId) {
        Optional.ofNullable(userId).orElseThrow(AccessDeniedException::new);
        @NotNull final SqlSession connection = connectionService.getSqlSession();
        @NotNull final IProjectRepository projectRepository = connection.getMapper(IProjectRepository.class);
        try {
            projectRepository.clearByUserId(userId);
            connection.commit();
        } catch (@NotNull final Exception e) {
            connection.rollback();
            throw e;
        } finally {
            connection.close();
        }
    }

    @NotNull
    @SneakyThrows
    @Override
    public Project findById(@Nullable final String userId, @Nullable final String id) {
        Optional.ofNullable(userId).orElseThrow(AccessDeniedException::new);
        Optional.ofNullable(id).filter((i) -> !i.isEmpty()).orElseThrow(EmptyIdException::new);
        @NotNull final SqlSession connection = connectionService.getSqlSession();
        @NotNull final IProjectRepository projectRepository = connection.getMapper(IProjectRepository.class);
        try {
            return Optional.ofNullable(projectRepository.findByIdByUserId(userId, id)).orElseThrow(EntityNotFoundException::new);
        } finally {
            connection.close();
        }
    }

    @NotNull
    @SneakyThrows
    @Override
    public Project findByName(@Nullable final String userId, @Nullable final String name) {
        Optional.ofNullable(userId).orElseThrow(AccessDeniedException::new);
        Optional.ofNullable(name).filter((i) -> !i.isEmpty()).orElseThrow(EmptyNameException::new);
        @NotNull final SqlSession connection = connectionService.getSqlSession();
        @NotNull final IProjectRepository projectRepository = connection.getMapper(IProjectRepository.class);
        try {
            return Optional.ofNullable(projectRepository.findByName(userId, name)).orElseThrow(EntityNotFoundException::new);
        } finally {
            connection.close();
        }
    }

    @NotNull
    @SneakyThrows
    @Override
    public Project removeById(@Nullable final String userId, @Nullable final String id) {
        Optional.ofNullable(userId).orElseThrow(AccessDeniedException::new);
        Optional.ofNullable(id).filter((i) -> !i.isEmpty()).orElseThrow(EmptyIdException::new);
        @NotNull final SqlSession connection = connectionService.getSqlSession();
        @NotNull final IProjectRepository projectRepository = connection.getMapper(IProjectRepository.class);
        try {
            @NotNull final Project project = Optional.ofNullable(projectRepository.findById(id)).orElseThrow(EntityNotFoundException::new);
            projectRepository.remove(project);
            connection.commit();
            return project;
        } catch (@NotNull final Exception e) {
            connection.rollback();
            throw e;
        } finally {
            connection.close();
        }
    }

    @NotNull
    @SneakyThrows
    @Override
    public Project removeByName(@Nullable final String userId, @Nullable final String name) {
        Optional.ofNullable(userId).orElseThrow(AccessDeniedException::new);
        Optional.ofNullable(name).filter((i) -> !i.isEmpty()).orElseThrow(EmptyNameException::new);
        @NotNull final SqlSession connection = connectionService.getSqlSession();
        @NotNull final IProjectRepository projectRepository = connection.getMapper(IProjectRepository.class);
        try {
            @NotNull final Project project = Optional.ofNullable(projectRepository.findByName(userId, name)).orElseThrow(EntityNotFoundException::new);
            projectRepository.remove(project);
            connection.commit();
            return project;
        } catch (@NotNull final Exception e) {
            connection.rollback();
            throw e;
        } finally {
            connection.close();
        }
    }

    @NotNull
    @SneakyThrows
    @Override
    public Project updateById(@Nullable final String userId, @Nullable final String id, @Nullable final String name, @Nullable final String description) {
        Optional.ofNullable(userId).orElseThrow(AccessDeniedException::new);
        Optional.ofNullable(id).filter((i) -> !i.isEmpty()).orElseThrow(EmptyIdException::new);
        Optional.ofNullable(name).filter((i) -> !i.isEmpty()).orElseThrow(EmptyNameException::new);
        @NotNull final SqlSession connection = connectionService.getSqlSession();
        @NotNull final IProjectRepository projectRepository = connection.getMapper(IProjectRepository.class);
        try {
            @NotNull final Project project = Optional.ofNullable(projectRepository.findByIdByUserId(userId, id)).orElseThrow(EntityNotFoundException::new);
            project.setName(name);
            project.setDescription(description);
            projectRepository.update(project);
            connection.commit();
            return project;
        } catch (@NotNull final Exception e) {
            connection.rollback();
            throw e;
        } finally {
            connection.close();
        }
    }

    @NotNull
    @SneakyThrows
    @Override
    public Project updateByName(@Nullable final String userId, @Nullable final String oldName, @Nullable final String name, @Nullable final String description) {
        Optional.ofNullable(userId).orElseThrow(AccessDeniedException::new);
        Optional.ofNullable(oldName).filter((i) -> !i.isEmpty()).orElseThrow(EmptyNameException::new);
        Optional.ofNullable(name).filter((i) -> !i.isEmpty()).orElseThrow(EmptyNameException::new);
        @NotNull final SqlSession connection = connectionService.getSqlSession();
        @NotNull final IProjectRepository projectRepository = connection.getMapper(IProjectRepository.class);
        try {
            @NotNull final Project project = Optional.ofNullable(projectRepository.findByName(userId, oldName)).orElseThrow(EntityNotFoundException::new);
            project.setName(name);
            project.setDescription(description);
            projectRepository.update(project);
            connection.commit();
            return project;
        } catch (@NotNull final Exception e) {
            connection.rollback();
            throw e;
        } finally {
            connection.close();
        }
    }

    @NotNull
    @SneakyThrows
    @Override
    public Project changeStatusById(@Nullable final String userId, @Nullable final String id, @NotNull final Status status) {
        Optional.ofNullable(userId).orElseThrow(AccessDeniedException::new);
        Optional.ofNullable(id).filter((i) -> !i.isEmpty()).orElseThrow(EmptyIdException::new);
        @NotNull final SqlSession connection = connectionService.getSqlSession();
        @NotNull final IProjectRepository projectRepository = connection.getMapper(IProjectRepository.class);
        try {
            @NotNull final Project project = Optional.ofNullable(projectRepository.findByIdByUserId(userId, id)).orElseThrow(EntityNotFoundException::new);
            project.setStatus(status);
            if (status == Status.IN_PROGRESS)
                project.setDateStart(new Date());
            projectRepository.update(project);
            connection.commit();
            return project;
        } catch (@NotNull final Exception e) {
            connection.rollback();
            throw e;
        } finally {
            connection.close();
        }
    }

    @NotNull
    @SneakyThrows
    @Override
    public Project changeStatusByName(@Nullable final String userId, @Nullable final String name, @NotNull final Status status) {
        Optional.ofNullable(userId).orElseThrow(AccessDeniedException::new);
        Optional.ofNullable(name).filter((i) -> !i.isEmpty()).orElseThrow(EmptyNameException::new);
        @NotNull final SqlSession connection = connectionService.getSqlSession();
        @NotNull final IProjectRepository projectRepository = connection.getMapper(IProjectRepository.class);
        try {
            @NotNull final Project project = Optional.ofNullable(projectRepository.findByName(userId, name)).orElseThrow(EntityNotFoundException::new);
            project.setStatus(status);
            if (status == Status.IN_PROGRESS)
                project.setDateStart(new Date());
            projectRepository.update(project);
            connection.commit();
            return project;
        } catch (@NotNull final Exception e) {
            connection.rollback();
            throw e;
        } finally {
            connection.close();
        }
    }

}
