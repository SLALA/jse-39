package ru.t1.strelcov.tm.api.repository;

import org.apache.ibatis.annotations.*;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.strelcov.tm.api.IBusinessRepository;
import ru.t1.strelcov.tm.model.Project;

import java.util.List;

public interface IProjectRepository extends IBusinessRepository<Project> {

    @NotNull
    @Select("SELECT id, user_id, name, description, status, created, start_date FROM tm_project")
    @Results(value = {
            @Result(property = "id", column = "id"),
            @Result(property = "userId", column = "user_id"),
            @Result(property = "name", column = "name"),
            @Result(property = "description", column = "description"),
            @Result(property = "status", column = "status"),
            @Result(property = "created", column = "created"),
            @Result(property = "dateStart", column = "start_date")
    })
    List<Project> findAll();

    @NotNull
    @Select("SELECT id, user_id, name, description, status, created, start_date FROM tm_project WHERE user_id = #{userId}")
    @Results(value = {
            @Result(property = "id", column = "id"),
            @Result(property = "userId", column = "user_id"),
            @Result(property = "name", column = "name"),
            @Result(property = "description", column = "description"),
            @Result(property = "status", column = "status"),
            @Result(property = "created", column = "created"),
            @Result(property = "dateStart", column = "start_date")
    })
    List<Project> findAllByUserId(@Param("userId") @NotNull final String userId);

    @NotNull
    @Select("SELECT id, user_id, name, description, status, created, start_date FROM tm_project WHERE user_id = #{userId} ORDER BY ${sortColumnName}")
    @Results(value = {
            @Result(property = "id", column = "id"),
            @Result(property = "userId", column = "user_id"),
            @Result(property = "name", column = "name"),
            @Result(property = "description", column = "description"),
            @Result(property = "status", column = "status"),
            @Result(property = "created", column = "created"),
            @Result(property = "dateStart", column = "start_date")
    })
    List<Project> findAllByUserIdSorted(@Param("userId") @NotNull final String userId, @Param("sortColumnName") @NotNull final String sortColumnName);

    @Insert("INSERT INTO tm_project(id, user_id, name, description, status, created, start_date)" +
            " VALUES(#{id}, #{userId}, #{name}, #{description}, #{status}, #{created}, #{dateStart})")
    void add(@NotNull final Project project);

    @Update("UPDATE tm_project" +
            " SET user_id = #{userId}, name = #{name}, description = #{description}, status = #{status}, created = #{created}, start_date = #{dateStart}" +
            " WHERE id = #{id}")
    void update(@NotNull final Project project);

    @Nullable
    @Select("SELECT id, user_id, name, description, status, created, start_date FROM tm_project WHERE id = #{id}")
    @Results(value = {
            @Result(property = "id", column = "id"),
            @Result(property = "userId", column = "user_id"),
            @Result(property = "name", column = "name"),
            @Result(property = "description", column = "description"),
            @Result(property = "status", column = "status"),
            @Result(property = "created", column = "created"),
            @Result(property = "dateStart", column = "start_date")
    })
    Project findById(@Param("id") @NotNull final String id);

    @Nullable
    @Select("SELECT id, user_id, name, description, status, created, start_date FROM tm_project WHERE user_id = #{userId} AND id = #{id}")
    @Results(value = {
            @Result(property = "id", column = "id"),
            @Result(property = "userId", column = "user_id"),
            @Result(property = "name", column = "name"),
            @Result(property = "description", column = "description"),
            @Result(property = "status", column = "status"),
            @Result(property = "created", column = "created"),
            @Result(property = "dateStart", column = "start_date")
    })
    Project findByIdByUserId(@Param("userId") @NotNull final String userId, @Param("id") @NotNull final String id);

    @Nullable
    @Select("SELECT id, user_id, name, description, status, created, start_date FROM tm_project WHERE user_id = #{userId} AND name = #{name} LIMIT 1")
    @Results(value = {
            @Result(property = "id", column = "id"),
            @Result(property = "userId", column = "user_id"),
            @Result(property = "name", column = "name"),
            @Result(property = "description", column = "description"),
            @Result(property = "status", column = "status"),
            @Result(property = "created", column = "created"),
            @Result(property = "dateStart", column = "start_date")
    })
    Project findByName(@Param("userId") @NotNull final String userId, @Param("name") @NotNull final String name);

    @Delete("DELETE FROM tm_project")
    void clear();

    @Delete("DELETE FROM tm_project WHERE user_id = #{userId}")
    void clearByUserId(@Param("userId") @NotNull final String userId);

    @Delete("DELETE FROM tm_project WHERE id = #{id}")
    void remove(@NotNull final Project project);

}
