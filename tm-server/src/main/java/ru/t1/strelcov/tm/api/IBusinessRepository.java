package ru.t1.strelcov.tm.api;

import ru.t1.strelcov.tm.model.AbstractBusinessEntity;

public interface IBusinessRepository<E extends AbstractBusinessEntity> extends IRepository<E> {

}
