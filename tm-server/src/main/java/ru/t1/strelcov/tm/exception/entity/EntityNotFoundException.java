package ru.t1.strelcov.tm.exception.entity;

import ru.t1.strelcov.tm.exception.AbstractException;

public final class EntityNotFoundException extends AbstractException {

    public EntityNotFoundException() {
        super("Error: Entity not found.");
    }

}
