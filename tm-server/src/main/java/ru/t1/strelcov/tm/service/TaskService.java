package ru.t1.strelcov.tm.service;

import lombok.SneakyThrows;
import org.apache.ibatis.session.SqlSession;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.strelcov.tm.api.repository.ITaskRepository;
import ru.t1.strelcov.tm.api.service.IConnectionService;
import ru.t1.strelcov.tm.api.service.ITaskService;
import ru.t1.strelcov.tm.enumerated.SortType;
import ru.t1.strelcov.tm.enumerated.Status;
import ru.t1.strelcov.tm.exception.empty.EmptyIdException;
import ru.t1.strelcov.tm.exception.empty.EmptyNameException;
import ru.t1.strelcov.tm.exception.entity.AccessDeniedException;
import ru.t1.strelcov.tm.exception.entity.EmptyProjectIdException;
import ru.t1.strelcov.tm.exception.entity.EmptyTaskIdException;
import ru.t1.strelcov.tm.exception.entity.EntityNotFoundException;
import ru.t1.strelcov.tm.exception.system.IncorrectSortOptionException;
import ru.t1.strelcov.tm.model.Task;

import java.util.Date;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.stream.Collectors;

public final class TaskService extends AbstractBusinessService<Task> implements ITaskService {

    @NotNull
    protected final IConnectionService connectionService;

    public TaskService(@NotNull final IConnectionService connectionService) {
        super(connectionService);
        this.connectionService = connectionService;
    }

    @NotNull
    @SneakyThrows
    @Override
    public Task add(@Nullable final String userId, @Nullable final String name, @Nullable final String description) {
        Optional.ofNullable(userId).orElseThrow(AccessDeniedException::new);
        Optional.ofNullable(name).filter((i) -> !i.isEmpty()).orElseThrow(EmptyNameException::new);
        final Task task;
        if (Optional.ofNullable(description).filter((i) -> !i.isEmpty()).isPresent())
            task = new Task(userId, name, description);
        else
            task = new Task(userId, name);
        add(task);
        return task;
    }

    @NotNull
    @SneakyThrows
    @Override
    public List<Task> findAllTasksByProjectId(@Nullable final String userId, @Nullable final String projectId) {
        Optional.ofNullable(userId).orElseThrow(AccessDeniedException::new);
        Optional.ofNullable(projectId).filter((i) -> !i.isEmpty()).orElseThrow(EmptyProjectIdException::new);
        @NotNull final SqlSession connection = connectionService.getSqlSession();
        @NotNull final ITaskRepository taskRepository = connection.getMapper(ITaskRepository.class);
        try {
            return taskRepository.findAllByProjectId(userId, projectId);
        } finally {
            connection.close();
        }
    }

    @NotNull
    @SneakyThrows
    @Override
    public Task bindTaskToProject(@Nullable final String userId, @Nullable final String taskId, @Nullable final String projectId) {
        Optional.ofNullable(userId).orElseThrow(AccessDeniedException::new);
        Optional.ofNullable(taskId).filter((i) -> !i.isEmpty()).orElseThrow(EmptyTaskIdException::new);
        Optional.ofNullable(projectId).filter((i) -> !i.isEmpty()).orElseThrow(EmptyProjectIdException::new);
        @NotNull final SqlSession connection = connectionService.getSqlSession();
        @NotNull final ITaskRepository taskRepository = connection.getMapper(ITaskRepository.class);
        try {
            @NotNull final Task task = Optional.ofNullable(taskRepository.findByIdByUserId(userId, taskId)).orElseThrow(EntityNotFoundException::new);
            task.setProjectId(projectId);
            taskRepository.update(task);
            connection.commit();
            return task;
        } catch (@NotNull final Exception e) {
            connection.rollback();
            throw e;
        } finally {
            connection.close();
        }
    }

    @NotNull
    @SneakyThrows
    @Override
    public Task unbindTaskFromProject(@Nullable final String userId, @Nullable final String taskId) {
        Optional.ofNullable(userId).orElseThrow(AccessDeniedException::new);
        Optional.ofNullable(taskId).filter((i) -> !i.isEmpty()).orElseThrow(EmptyIdException::new);
        @NotNull final SqlSession connection = connectionService.getSqlSession();
        @NotNull final ITaskRepository taskRepository = connection.getMapper(ITaskRepository.class);
        try {
            @NotNull final Task task = Optional.ofNullable(taskRepository.findByIdByUserId(userId, taskId)).orElseThrow(EntityNotFoundException::new);
            task.setProjectId(null);
            taskRepository.update(task);
            connection.commit();
            return task;
        } catch (@NotNull final Exception e) {
            connection.rollback();
            throw e;
        } finally {
            connection.close();
        }
    }

    @NotNull
    @SneakyThrows
    @Override
    public List<Task> findAll() {
        @NotNull final SqlSession connection = connectionService.getSqlSession();
        @NotNull final ITaskRepository taskRepository = connection.getMapper(ITaskRepository.class);
        try {
            return taskRepository.findAll();
        } finally {
            connection.close();
        }
    }

    @SneakyThrows
    @Override
    public void add(@Nullable final Task task) {
        if (task == null) return;
        @NotNull final SqlSession connection = connectionService.getSqlSession();
        @NotNull final ITaskRepository taskRepository = connection.getMapper(ITaskRepository.class);
        try {
            taskRepository.add(task);
            connection.commit();
        } catch (@NotNull final Exception e) {
            connection.rollback();
            throw e;
        } finally {
            connection.close();
        }
    }

    @SneakyThrows
    @Override
    public void addAll(@Nullable final List<Task> list) {
        @Nullable List<Task> listNonNull = Optional.ofNullable(list).map(l -> l.stream().filter(Objects::nonNull).collect(Collectors.toList())).orElse(null);
        if (listNonNull == null || listNonNull.size() == 0) return;
        @NotNull final SqlSession connection = connectionService.getSqlSession();
        @NotNull final ITaskRepository taskRepository = connection.getMapper(ITaskRepository.class);
        try {
            listNonNull.forEach(taskRepository::add);
            connection.commit();
        } catch (@NotNull final Exception e) {
            connection.rollback();
            throw e;
        } finally {
            connection.close();
        }
    }

    @SneakyThrows
    @Override
    public void clear() {
        @NotNull final SqlSession connection = connectionService.getSqlSession();
        @NotNull final ITaskRepository taskRepository = connection.getMapper(ITaskRepository.class);
        try {
            taskRepository.clear();
            connection.commit();
        } catch (@NotNull final Exception e) {
            connection.rollback();
            throw e;
        } finally {
            connection.close();
        }
    }

    @NotNull
    @SneakyThrows
    @Override
    public Task findById(@Nullable final String id) {
        Optional.ofNullable(id).filter((i) -> !i.isEmpty()).orElseThrow(EmptyIdException::new);
        @NotNull final SqlSession connection = connectionService.getSqlSession();
        @NotNull final ITaskRepository taskRepository = connection.getMapper(ITaskRepository.class);
        try {
            return Optional.ofNullable(taskRepository.findById(id)).orElseThrow(EntityNotFoundException::new);
        } finally {
            connection.close();
        }
    }

    @NotNull
    @SneakyThrows
    @Override
    public Task removeById(@Nullable final String id) {
        Optional.ofNullable(id).filter((i) -> !i.isEmpty()).orElseThrow(EmptyIdException::new);
        @NotNull final SqlSession connection = connectionService.getSqlSession();
        @NotNull final ITaskRepository taskRepository = connection.getMapper(ITaskRepository.class);
        try {
            final Task task = Optional.ofNullable(taskRepository.findById(id)).orElseThrow(EntityNotFoundException::new);
            taskRepository.remove(task);
            connection.commit();
            return task;
        } catch (@NotNull final Exception e) {
            connection.rollback();
            throw e;
        } finally {
            connection.close();
        }
    }

    @NotNull
    @SneakyThrows
    @Override
    public List<Task> findAll(@Nullable final String userId) {
        Optional.ofNullable(userId).orElseThrow(AccessDeniedException::new);
        @NotNull final SqlSession connection = connectionService.getSqlSession();
        @NotNull final ITaskRepository taskRepository = connection.getMapper(ITaskRepository.class);
        try {
            return taskRepository.findAllByUserId(userId);
        } finally {
            connection.close();
        }
    }

    @NotNull
    @SneakyThrows
    @Override
    public List<Task> findAll(@Nullable final String userId, @Nullable final String sort) {
        Optional.ofNullable(userId).orElseThrow(AccessDeniedException::new);
        Optional.ofNullable(sort).orElseThrow(() -> {
            throw new IncorrectSortOptionException(sort);
        });
        if (!SortType.isValidByName(sort)) throw new IncorrectSortOptionException(sort);
        @NotNull final SqlSession connection = connectionService.getSqlSession();
        @NotNull final ITaskRepository taskRepository = connection.getMapper(ITaskRepository.class);
        try {
            return taskRepository.findAllByUserIdSorted(userId, SortType.valueOf(sort).getDataBaseName());
        } finally {
            connection.close();
        }
    }

    @SneakyThrows
    @Override
    public void clear(@Nullable final String userId) {
        Optional.ofNullable(userId).orElseThrow(AccessDeniedException::new);
        @NotNull final SqlSession connection = connectionService.getSqlSession();
        @NotNull final ITaskRepository taskRepository = connection.getMapper(ITaskRepository.class);
        try {
            taskRepository.clearByUserId(userId);
            connection.commit();
        } catch (@NotNull final Exception e) {
            connection.rollback();
            throw e;
        } finally {
            connection.close();
        }
    }

    @NotNull
    @SneakyThrows
    @Override
    public Task findById(@Nullable final String userId, @Nullable final String id) {
        Optional.ofNullable(userId).orElseThrow(AccessDeniedException::new);
        Optional.ofNullable(id).filter((i) -> !i.isEmpty()).orElseThrow(EmptyIdException::new);
        @NotNull final SqlSession connection = connectionService.getSqlSession();
        @NotNull final ITaskRepository taskRepository = connection.getMapper(ITaskRepository.class);
        try {
            return Optional.ofNullable(taskRepository.findByIdByUserId(userId, id)).orElseThrow(EntityNotFoundException::new);
        } finally {
            connection.close();
        }
    }

    @NotNull
    @SneakyThrows
    @Override
    public Task findByName(@Nullable final String userId, @Nullable final String name) {
        Optional.ofNullable(userId).orElseThrow(AccessDeniedException::new);
        Optional.ofNullable(name).filter((i) -> !i.isEmpty()).orElseThrow(EmptyNameException::new);
        @NotNull final SqlSession connection = connectionService.getSqlSession();
        @NotNull final ITaskRepository taskRepository = connection.getMapper(ITaskRepository.class);
        try {
            return Optional.ofNullable(taskRepository.findByName(userId, name)).orElseThrow(EntityNotFoundException::new);
        } finally {
            connection.close();
        }
    }

    @NotNull
    @SneakyThrows
    @Override
    public Task removeById(@Nullable final String userId, @Nullable final String id) {
        Optional.ofNullable(userId).orElseThrow(AccessDeniedException::new);
        Optional.ofNullable(id).filter((i) -> !i.isEmpty()).orElseThrow(EmptyIdException::new);
        @NotNull final SqlSession connection = connectionService.getSqlSession();
        @NotNull final ITaskRepository taskRepository = connection.getMapper(ITaskRepository.class);
        try {
            @NotNull final Task task = Optional.ofNullable(taskRepository.findByIdByUserId(userId, id)).orElseThrow(EntityNotFoundException::new);
            taskRepository.remove(task);
            connection.commit();
            return task;
        } catch (@NotNull final Exception e) {
            connection.rollback();
            throw e;
        } finally {
            connection.close();
        }
    }

    @NotNull
    @SneakyThrows
    @Override
    public Task removeByName(@Nullable final String userId, @Nullable final String name) {
        Optional.ofNullable(userId).orElseThrow(AccessDeniedException::new);
        Optional.ofNullable(name).filter((i) -> !i.isEmpty()).orElseThrow(EmptyNameException::new);
        @NotNull final SqlSession connection = connectionService.getSqlSession();
        @NotNull final ITaskRepository taskRepository = connection.getMapper(ITaskRepository.class);
        try {
            @NotNull final Task task = Optional.ofNullable(taskRepository.findByName(userId, name)).orElseThrow(EntityNotFoundException::new);
            taskRepository.remove(task);
            connection.commit();
            return task;
        } catch (@NotNull final Exception e) {
            connection.rollback();
            throw e;
        } finally {
            connection.close();
        }
    }

    @NotNull
    @SneakyThrows
    @Override
    public Task updateById(@Nullable final String userId, @Nullable final String id, @Nullable final String name, @Nullable final String description) {
        Optional.ofNullable(userId).orElseThrow(AccessDeniedException::new);
        Optional.ofNullable(id).filter((i) -> !i.isEmpty()).orElseThrow(EmptyIdException::new);
        Optional.ofNullable(name).filter((i) -> !i.isEmpty()).orElseThrow(EmptyNameException::new);
        @NotNull final SqlSession connection = connectionService.getSqlSession();
        @NotNull final ITaskRepository taskRepository = connection.getMapper(ITaskRepository.class);
        try {
            @NotNull final Task task = Optional.ofNullable(taskRepository.findByIdByUserId(userId, id)).orElseThrow(EntityNotFoundException::new);
            task.setName(name);
            task.setDescription(description);
            taskRepository.update(task);
            connection.commit();
            return task;
        } catch (@NotNull final Exception e) {
            connection.rollback();
            throw e;
        } finally {
            connection.close();
        }
    }

    @NotNull
    @SneakyThrows
    @Override
    public Task updateByName(@Nullable final String userId, @Nullable final String oldName, @Nullable final String name, @Nullable final String description) {
        Optional.ofNullable(userId).orElseThrow(AccessDeniedException::new);
        Optional.ofNullable(oldName).filter((i) -> !i.isEmpty()).orElseThrow(EmptyNameException::new);
        Optional.ofNullable(name).filter((i) -> !i.isEmpty()).orElseThrow(EmptyNameException::new);
        @NotNull final SqlSession connection = connectionService.getSqlSession();
        @NotNull final ITaskRepository taskRepository = connection.getMapper(ITaskRepository.class);
        try {
            @NotNull final Task task = Optional.ofNullable(taskRepository.findByName(userId, oldName)).orElseThrow(EntityNotFoundException::new);
            task.setName(name);
            task.setDescription(description);
            taskRepository.update(task);
            connection.commit();
            return task;
        } catch (@NotNull final Exception e) {
            connection.rollback();
            throw e;
        } finally {
            connection.close();
        }
    }

    @NotNull
    @SneakyThrows
    @Override
    public Task changeStatusById(@Nullable final String userId, @Nullable final String id, @NotNull final Status status) {
        Optional.ofNullable(userId).orElseThrow(AccessDeniedException::new);
        Optional.ofNullable(id).filter((i) -> !i.isEmpty()).orElseThrow(EmptyIdException::new);
        @NotNull final SqlSession connection = connectionService.getSqlSession();
        @NotNull final ITaskRepository taskRepository = connection.getMapper(ITaskRepository.class);
        try {
            @NotNull final Task task = Optional.ofNullable(taskRepository.findByIdByUserId(userId, id)).orElseThrow(EntityNotFoundException::new);
            task.setStatus(status);
            if (status == Status.IN_PROGRESS)
                task.setDateStart(new Date());
            taskRepository.update(task);
            connection.commit();
            return task;
        } catch (@NotNull final Exception e) {
            connection.rollback();
            throw e;
        } finally {
            connection.close();
        }
    }

    @NotNull
    @SneakyThrows
    @Override
    public Task changeStatusByName(@Nullable final String userId, @Nullable final String name, @NotNull final Status status) {
        Optional.ofNullable(userId).orElseThrow(AccessDeniedException::new);
        Optional.ofNullable(name).filter((i) -> !i.isEmpty()).orElseThrow(EmptyNameException::new);
        @NotNull final SqlSession connection = connectionService.getSqlSession();
        @NotNull final ITaskRepository taskRepository = connection.getMapper(ITaskRepository.class);
        try {
            @NotNull final Task task = Optional.ofNullable(taskRepository.findByName(userId, name)).orElseThrow(EntityNotFoundException::new);
            task.setStatus(status);
            if (status == Status.IN_PROGRESS)
                task.setDateStart(new Date());
            taskRepository.update(task);
            connection.commit();
            return task;
        } catch (@NotNull final Exception e) {
            connection.rollback();
            throw e;
        } finally {
            connection.close();
        }
    }

}
