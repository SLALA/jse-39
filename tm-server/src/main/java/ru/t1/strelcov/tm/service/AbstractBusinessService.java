package ru.t1.strelcov.tm.service;

import org.jetbrains.annotations.NotNull;
import ru.t1.strelcov.tm.api.IBusinessService;
import ru.t1.strelcov.tm.api.service.IConnectionService;
import ru.t1.strelcov.tm.model.AbstractBusinessEntity;

public abstract class AbstractBusinessService<E extends AbstractBusinessEntity> extends AbstractService<E> implements IBusinessService<E> {

    public AbstractBusinessService(@NotNull final IConnectionService connectionService) {
        super(connectionService);
    }

}
