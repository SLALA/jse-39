package ru.t1.strelcov.tm.service;

import lombok.SneakyThrows;
import org.apache.ibatis.session.SqlSession;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.strelcov.tm.api.repository.IUserRepository;
import ru.t1.strelcov.tm.api.service.IConnectionService;
import ru.t1.strelcov.tm.api.service.IPropertyService;
import ru.t1.strelcov.tm.api.service.IUserService;
import ru.t1.strelcov.tm.enumerated.Role;
import ru.t1.strelcov.tm.exception.empty.EmptyIdException;
import ru.t1.strelcov.tm.exception.empty.EmptyLoginException;
import ru.t1.strelcov.tm.exception.empty.EmptyPasswordException;
import ru.t1.strelcov.tm.exception.entity.EntityNotFoundException;
import ru.t1.strelcov.tm.exception.entity.UserAdminLockException;
import ru.t1.strelcov.tm.exception.entity.UserLoginExistsException;
import ru.t1.strelcov.tm.exception.entity.UserNotFoundException;
import ru.t1.strelcov.tm.model.User;
import ru.t1.strelcov.tm.util.HashUtil;

import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.stream.Collectors;

public final class UserService extends AbstractService<User> implements IUserService {

    @NotNull
    protected final IConnectionService connectionService;

    @NotNull
    protected final IPropertyService propertyService;

    public UserService(@NotNull final IConnectionService connectionService, @NotNull IPropertyService propertyService) {
        super(connectionService);
        this.connectionService = connectionService;
        this.propertyService = propertyService;
    }

    @NotNull
    @SneakyThrows
    @Override
    public User add(@Nullable final String login, @Nullable final String password) {
        Optional.ofNullable(login).filter((i) -> !i.isEmpty()).orElseThrow(EmptyLoginException::new);
        Optional.ofNullable(password).filter((i) -> !i.isEmpty()).orElseThrow(EmptyPasswordException::new);
        @NotNull final String passwordHash = HashUtil.salt(propertyService.getPasswordSecret(), propertyService.getPasswordIteration(), password);
        @NotNull final User user = new User(login, passwordHash);
        add(user);
        return user;
    }

    @NotNull
    @SneakyThrows
    @Override
    public User add(@Nullable final String login, @Nullable final String password, @Nullable final String email) {
        Optional.ofNullable(login).filter((i) -> !i.isEmpty()).orElseThrow(EmptyLoginException::new);
        Optional.ofNullable(password).filter((i) -> !i.isEmpty()).orElseThrow(EmptyPasswordException::new);
        @NotNull final String passwordHash = HashUtil.salt(propertyService.getPasswordSecret(), propertyService.getPasswordIteration(), password);
        @NotNull final User user = new User(login, passwordHash, email);
        add(user);
        return user;
    }

    @NotNull
    @SneakyThrows
    @Override
    public User add(@Nullable final String login, @Nullable final String password, @Nullable final Role role) {
        Optional.ofNullable(login).filter((i) -> !i.isEmpty()).orElseThrow(EmptyLoginException::new);
        Optional.ofNullable(password).filter((i) -> !i.isEmpty()).orElseThrow(EmptyPasswordException::new);
        @NotNull final User user;
        @NotNull final String passwordHash = HashUtil.salt(propertyService.getPasswordSecret(), propertyService.getPasswordIteration(), password);
        user = new User(login, passwordHash, role);
        add(user);
        return user;
    }

    @NotNull
    @SneakyThrows
    @Override
    public User findByLogin(@Nullable final String login) {
        Optional.ofNullable(login).filter((i) -> !i.isEmpty()).orElseThrow(EmptyLoginException::new);
        @NotNull final SqlSession connection = connectionService.getSqlSession();
        @NotNull final IUserRepository userRepository = connection.getMapper(IUserRepository.class);
        try {
            return Optional.ofNullable(userRepository.findByLogin(login)).orElseThrow(EntityNotFoundException::new);
        } finally {
            connection.close();
        }
    }

    @NotNull
    @SneakyThrows
    @Override
    public User removeByLogin(@Nullable final String login) {
        Optional.ofNullable(login).filter((i) -> !i.isEmpty()).orElseThrow(EmptyLoginException::new);
        @NotNull final SqlSession connection = connectionService.getSqlSession();
        @NotNull final IUserRepository userRepository = connection.getMapper(IUserRepository.class);
        try {
            @NotNull final User user = Optional.ofNullable(userRepository.findByLogin(login)).orElseThrow(UserNotFoundException::new);
            userRepository.remove(user);
            connection.commit();
            return user;
        } catch (@NotNull final Exception e) {
            connection.rollback();
            throw e;
        } finally {
            connection.close();
        }
    }

    @NotNull
    @SneakyThrows
    @Override
    public User updateById(@Nullable final String id, @Nullable final String firstName, @Nullable final String lastName, @Nullable final String middleName, @Nullable final String email) {
        Optional.ofNullable(id).filter((i) -> !i.isEmpty()).orElseThrow(EmptyIdException::new);
        @NotNull final SqlSession connection = connectionService.getSqlSession();
        @NotNull final IUserRepository userRepository = connection.getMapper(IUserRepository.class);
        try {
            @NotNull final User user = Optional.ofNullable(userRepository.findById(id)).orElseThrow(UserNotFoundException::new);
            user.setFirstName(firstName);
            user.setLastName(lastName);
            user.setMiddleName(middleName);
            user.setEmail(email);
            userRepository.update(user);
            connection.commit();
            return user;
        } catch (@NotNull final Exception e) {
            connection.rollback();
            throw e;
        } finally {
            connection.close();
        }
    }

    @NotNull
    @SneakyThrows
    @Override
    public User updateByLogin(@Nullable final String login, @Nullable final String firstName, @Nullable final String lastName, @Nullable final String middleName, @Nullable final String email) {
        Optional.ofNullable(login).filter((i) -> !i.isEmpty()).orElseThrow(EmptyLoginException::new);
        @NotNull final SqlSession connection = connectionService.getSqlSession();
        @NotNull final IUserRepository userRepository = connection.getMapper(IUserRepository.class);
        try {
            @NotNull final User user = Optional.ofNullable(userRepository.findByLogin(login)).orElseThrow(UserNotFoundException::new);
            user.setFirstName(firstName);
            user.setLastName(lastName);
            user.setMiddleName(middleName);
            user.setEmail(email);
            userRepository.update(user);
            connection.commit();
            return user;
        } catch (@NotNull final Exception e) {
            connection.rollback();
            throw e;
        } finally {
            connection.close();
        }
    }

    @SneakyThrows
    @Override
    public void changePasswordById(@Nullable final String id, @Nullable final String password) {
        Optional.ofNullable(id).filter((i) -> !i.isEmpty()).orElseThrow(EmptyIdException::new);
        Optional.ofNullable(password).filter((i) -> !i.isEmpty()).orElseThrow(EmptyPasswordException::new);
        @NotNull final SqlSession connection = connectionService.getSqlSession();
        @NotNull final IUserRepository userRepository = connection.getMapper(IUserRepository.class);
        try {
            @NotNull final User user = Optional.ofNullable(userRepository.findById(id)).orElseThrow(UserNotFoundException::new);
            final String passwordHash = HashUtil.salt(propertyService.getPasswordSecret(), propertyService.getPasswordIteration(), password);
            user.setPasswordHash(passwordHash);
            userRepository.update(user);
            connection.commit();
        } catch (@NotNull final Exception e) {
            connection.rollback();
            throw e;
        } finally {
            connection.close();
        }
    }

    @SneakyThrows
    @Override
    public void lockUserByLogin(@Nullable final String login) {
        Optional.ofNullable(login).filter((i) -> !i.isEmpty()).orElseThrow(EmptyLoginException::new);
        @NotNull final SqlSession connection = connectionService.getSqlSession();
        @NotNull final IUserRepository userRepository = connection.getMapper(IUserRepository.class);
        try {
            @NotNull final User user = Optional.ofNullable(userRepository.findByLogin(login)).orElseThrow(UserNotFoundException::new);
            if (user.getRole() == Role.ADMIN) throw new UserAdminLockException();
            user.setLock(true);
            userRepository.update(user);
            connection.commit();
        } catch (@NotNull final Exception e) {
            connection.rollback();
            throw e;
        } finally {
            connection.close();
        }
    }

    @SneakyThrows
    @Override
    public void unlockUserByLogin(@Nullable final String login) {
        Optional.ofNullable(login).filter((i) -> !i.isEmpty()).orElseThrow(EmptyLoginException::new);
        @NotNull final SqlSession connection = connectionService.getSqlSession();
        @NotNull final IUserRepository userRepository = connection.getMapper(IUserRepository.class);
        try {
            @NotNull final User user = Optional.ofNullable(userRepository.findByLogin(login)).orElseThrow(UserNotFoundException::new);
            user.setLock(false);
            userRepository.update(user);
            connection.commit();
        } catch (@NotNull final Exception e) {
            connection.rollback();
            throw e;
        } finally {
            connection.close();
        }
    }

    @NotNull
    @SneakyThrows
    @Override
    public List<User> findAll() {
        @NotNull final SqlSession connection = connectionService.getSqlSession();
        @NotNull final IUserRepository userRepository = connection.getMapper(IUserRepository.class);
        try {
            return userRepository.findAll();
        } finally {
            connection.close();
        }
    }

    @SneakyThrows
    @Override
    public void add(@Nullable final User user) {
        if (user == null) return;
        @NotNull final SqlSession connection = connectionService.getSqlSession();
        @NotNull final IUserRepository userRepository = connection.getMapper(IUserRepository.class);
        try {
            if (userRepository.loginExists(user.getLogin())) throw new UserLoginExistsException();
            userRepository.add(user);
            connection.commit();
        } catch (@NotNull final Exception e) {
            connection.rollback();
            throw e;
        } finally {
            connection.close();
        }
    }

    @SneakyThrows
    @Override
    public void addAll(@Nullable final List<User> list) {
        @Nullable List<User> listNonNull = Optional.ofNullable(list).map(l -> l.stream().filter(Objects::nonNull).collect(Collectors.toList())).orElse(null);
        if (listNonNull == null || listNonNull.size() == 0) return;
        @NotNull final SqlSession connection = connectionService.getSqlSession();
        @NotNull final IUserRepository userRepository = connection.getMapper(IUserRepository.class);
        try {
            listNonNull.forEach(userRepository::add);
            connection.commit();
        } catch (@NotNull final Exception e) {
            connection.rollback();
            throw e;
        } finally {
            connection.close();
        }
    }

    @SneakyThrows
    @Override
    public void clear() {
        @NotNull final SqlSession connection = connectionService.getSqlSession();
        @NotNull final IUserRepository userRepository = connection.getMapper(IUserRepository.class);
        try {
            userRepository.clear();
            connection.commit();
        } catch (@NotNull final Exception e) {
            connection.rollback();
            throw e;
        } finally {
            connection.close();
        }
    }

    @NotNull
    @SneakyThrows
    @Override
    public User findById(@Nullable final String id) {
        Optional.ofNullable(id).filter((i) -> !i.isEmpty()).orElseThrow(EmptyIdException::new);
        @NotNull final SqlSession connection = connectionService.getSqlSession();
        @NotNull final IUserRepository userRepository = connection.getMapper(IUserRepository.class);
        try {
            return Optional.ofNullable(userRepository.findById(id)).orElseThrow(EntityNotFoundException::new);
        } finally {
            connection.close();
        }
    }

    @NotNull
    @SneakyThrows
    @Override
    public User removeById(@Nullable final String id) {
        Optional.ofNullable(id).filter((i) -> !i.isEmpty()).orElseThrow(EmptyIdException::new);
        @NotNull final SqlSession connection = connectionService.getSqlSession();
        @NotNull final IUserRepository userRepository = connection.getMapper(IUserRepository.class);
        try {
            final User user = Optional.ofNullable(userRepository.findById(id)).orElseThrow(EntityNotFoundException::new);
            userRepository.remove(user);
            connection.commit();
            return user;
        } catch (@NotNull final Exception e) {
            connection.rollback();
            throw e;
        } finally {
            connection.close();
        }
    }

}
